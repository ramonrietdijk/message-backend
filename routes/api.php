<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\FeedController;
use App\Http\Controllers\LikeController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SettingController;
use App\Http\Controllers\SubscriptionController;
use App\Http\Middleware\TokenMiddleware;
use Illuminate\Support\Facades\Route;

Route::prefix('v1')->group(function (): void {
    Route::prefix('auth')->group(function (): void {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('register', [AuthController::class, 'register']);

        Route::middleware(TokenMiddleware::class)->group(function (): void {
            Route::post('refresh', [AuthController::class, 'refreshToken']);
            Route::get('user', [AuthController::class, 'user']);
        });
    });

    Route::middleware(TokenMiddleware::class)->group(function (): void {
        Route::prefix('profile/{profile}')->group(function (): void {
            Route::get('', [ProfileController::class, 'show']);
            Route::post('subscribe', [SubscriptionController::class, 'subscribe']);
            Route::post('unsubscribe', [SubscriptionController::class, 'unsubscribe']);
        });

        Route::prefix('settings')->group(function (): void {
            Route::patch('general', [SettingController::class, 'updateGeneral']);
            Route::post('picture', [SettingController::class, 'updatePicture']);
            Route::delete('picture', [SettingController::class, 'deletePicture']);
            Route::patch('email', [SettingController::class, 'updateEmail']);
            Route::patch('password', [SettingController::class, 'updatePassword']);
            Route::patch('privacy', [SettingController::class, 'updatePrivacy']);
        });

        Route::prefix('messages')->group(function (): void {
            Route::get('feed/{limit}/{last}', [FeedController::class, 'feed']);
            Route::get('user/{profile}/{limit}/{last}', [FeedController::class, 'user']);

            Route::post('', [MessageController::class, 'create']);

            Route::prefix('{message}')->group(function (): void {
                Route::get('', [MessageController::class, 'show']);
                Route::patch('', [MessageController::class, 'update']);
                Route::delete('', [MessageController::class, 'delete']);

                Route::post('like', [LikeController::class, 'message']);
            });
        });
    });
});
