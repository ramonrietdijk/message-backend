<?php

use Illuminate\Support\Facades\Route;

Route::get('/', fn (): string => app()->version());
