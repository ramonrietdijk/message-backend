<?php

return [

    'algorithm' => 'HS512',

    'secret' => env('JWT_SECRET'),

    'expire' => env('JWT_EXPIRE', 900),

];
