<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\Response;

class UserPolicy
{
    public function feed(User $user, User $model): Response
    {
        return new Response(
            $model->is_public || $user->id === $model->id,
            __('You don\'t have permission to see these messages')
        );
    }
}
