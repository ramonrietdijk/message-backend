<?php

namespace App\Policies;

use App\Models\Message;
use App\Models\User;
use Illuminate\Auth\Access\Response;

class MessagePolicy
{
    public function view(User $user, Message $message): Response
    {
        return new Response(
            $user->id === $message->user_id || $message->user?->is_public,
            __('You can\'t view that message')
        );
    }

    public function create(User $user): Response
    {
        return Response::allow();
    }

    public function update(User $user, Message $message): Response
    {
        return new Response($user->id === $message->user_id, __('You can\'t update that message'));
    }

    public function delete(User $user, Message $message): Response
    {
        return new Response($user->id === $message->user_id, __('You can\'t delete that message'));
    }
}
