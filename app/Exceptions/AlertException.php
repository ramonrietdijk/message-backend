<?php

namespace App\Exceptions;

use App\Utilities\AlertMessage;
use Exception;
use Illuminate\Http\JsonResponse;

class AlertException extends Exception
{
    public function render(): JsonResponse
    {
        return AlertMessage::danger($this->message)->asJsonResponse($this->code);
    }
}
