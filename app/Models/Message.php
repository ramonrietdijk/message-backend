<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Enumerable;

/**
 * @property int $id
 * @property int $user_id
 * @property string $body
 * @property ?Carbon $created_at
 * @property ?Carbon $updated_at
 * @property ?User $user
 * @property Enumerable<int, Like> $likes
 */
class Message extends Model
{
    use HasFactory;

    /**
     * @return BelongsTo<User, Message>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    /**
     * @return MorphMany<Like>
     */
    public function likes(): MorphMany
    {
        return $this->morphMany(Like::class, 'likeable');
    }
}
