<?php

namespace App\Models;

use App\Concerns\HasPicture;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Enumerable;

/**
 * @property int $id
 * @property string $email
 * @property string $username
 * @property string $password
 * @property string $firstname
 * @property string $lastname
 * @property string $motto
 * @property ?string $picture
 * @property ?int $picture_time
 * @property bool $is_public
 * @property ?Carbon $created_at
 * @property ?Carbon $updated_at
 * @property Enumerable<int, Message> $messages
 * @property Enumerable<int, Subscription> $subscribers
 * @property Enumerable<int, Subscription> $subscriptions
 */
class User extends Authenticatable
{
    use HasFactory;
    use HasPicture;

    /**
     * @return array<string, string>
     */
    protected function casts(): array
    {
        return [
            'is_public' => 'bool',
        ];
    }

    public function getRouteKeyName(): string
    {
        return 'username';
    }

    /**
     * @return HasMany<Message>
     */
    public function messages(): HasMany
    {
        return $this->hasMany(Message::class, 'user_id', 'id');
    }

    /**
     * @return HasManyThrough<User>
     */
    public function subscribers(): HasManyThrough
    {
        return $this->hasManyThrough(
            User::class,
            Subscription::class,
            'related_id',
            'id',
            'id',
            'user_id'
        );
    }

    /**
     * @return HasManyThrough<User>
     */
    public function subscriptions(): HasManyThrough
    {
        return $this->hasManyThrough(
            User::class,
            Subscription::class,
            'user_id',
            'id',
            'id',
            'related_id'
        );
    }
}
