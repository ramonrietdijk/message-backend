<?php

namespace App\Services;

use App\Models\User;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class JwtService
{
    public function __construct(
        private readonly string $secret,
        private readonly string $algorithm,
        private readonly int $expire
    ) {
    }

    public function encode(User $user): string
    {
        $time = now()->getTimestamp();

        $payload = [
            'iss' => 'laravel',
            'sub' => $user->id,
            'iat' => $time,
            'exp' => $time + $this->expire,
        ];

        return JWT::encode($payload, $this->secret, $this->algorithm);
    }

    public function decode(string $token): User
    {
        $key = new Key($this->secret, $this->algorithm);

        $payload = JWT::decode($token, $key);

        /** @var User $user */
        $user = User::query()->findOrFail($payload->sub);

        return $user;
    }
}
