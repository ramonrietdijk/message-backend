<?php

namespace App\Http\Responses\Like;

use App\Http\Resources\MessageResource;
use App\Models\Message;
use Illuminate\Http\JsonResponse;

class LikeMessageResponse
{
    public function make(Message $message): JsonResponse
    {
        return response()->json([
            'message' => MessageResource::make($message),
        ]);
    }
}
