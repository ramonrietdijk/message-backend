<?php

namespace App\Http\Responses\Message;

use Illuminate\Http\JsonResponse;

class DeleteMessageResponse
{
    public function make(bool $deleted): JsonResponse
    {
        return response()->json();
    }
}
