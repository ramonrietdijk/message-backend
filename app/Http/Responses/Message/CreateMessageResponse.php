<?php

namespace App\Http\Responses\Message;

use App\Http\Resources\MessageResource;
use App\Models\Message;
use Illuminate\Http\JsonResponse;

class CreateMessageResponse
{
    public function make(Message $message): JsonResponse
    {
        return response()->json([
            'message' => MessageResource::make($message),
        ]);
    }
}
