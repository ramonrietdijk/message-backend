<?php

namespace App\Http\Responses\Auth;

use Illuminate\Http\JsonResponse;

class RefreshTokenResponse
{
    public function make(string $token): JsonResponse
    {
        return response()->json([
            'token' => $token,
        ]);
    }
}
