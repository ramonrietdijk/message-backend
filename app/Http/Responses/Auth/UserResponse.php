<?php

namespace App\Http\Responses\Auth;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class UserResponse
{
    public function make(User $user): JsonResponse
    {
        return response()->json([
            'user' => UserResource::make($user),
        ]);
    }
}
