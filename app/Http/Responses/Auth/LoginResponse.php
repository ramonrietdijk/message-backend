<?php

namespace App\Http\Responses\Auth;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class LoginResponse
{
    public function make(User $user, string $token): JsonResponse
    {
        return response()->json([
            'user' => UserResource::make($user),
            'token' => $token,
        ]);
    }
}
