<?php

namespace App\Http\Responses\Auth;

use App\Models\User;
use App\Utilities\AlertMessage;
use Illuminate\Http\JsonResponse;

class RegisterResponse
{
    public function make(User $user): JsonResponse
    {
        return AlertMessage::success(__('Your account has been created successfully, :username!', [
            'username' => $user->username,
        ]))->asJsonResponse();
    }
}
