<?php

namespace App\Http\Responses\Subscription;

use App\Http\Resources\ProfileResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class UnsubscriptionResponse
{
    public function make(User $profile): JsonResponse
    {
        return response()->json([
            'profile' => ProfileResource::make($profile),
        ]);
    }
}
