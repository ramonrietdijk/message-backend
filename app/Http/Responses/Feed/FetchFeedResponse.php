<?php

namespace App\Http\Responses\Feed;

use App\Http\Resources\MessageResource;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Enumerable;

class FetchFeedResponse
{
    /**
     * @param  Enumerable<int, Model>  $messages
     */
    public function make(Enumerable $messages): JsonResponse
    {
        return response()->json([
            'messages' => MessageResource::collection($messages),
        ]);
    }
}
