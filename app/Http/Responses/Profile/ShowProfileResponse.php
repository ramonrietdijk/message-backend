<?php

namespace App\Http\Responses\Profile;

use App\Http\Resources\ProfileResource;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class ShowProfileResponse
{
    public function make(User $profile): JsonResponse
    {
        return response()->json([
            'profile' => ProfileResource::make($profile),
        ]);
    }
}
