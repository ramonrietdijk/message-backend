<?php

namespace App\Http\Responses\Setting;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Utilities\AlertMessage;
use Illuminate\Http\JsonResponse;

class UpdatePasswordResponse
{
    public function make(User $user): JsonResponse
    {
        return response()->json([
            'alertMessage' => AlertMessage::success(__('Your password has been updated successfully!'))->toArray(),
            'user' => UserResource::make($user),
        ]);
    }
}
