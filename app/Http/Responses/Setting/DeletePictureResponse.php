<?php

namespace App\Http\Responses\Setting;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Utilities\AlertMessage;
use Illuminate\Http\JsonResponse;

class DeletePictureResponse
{
    public function make(User $user): JsonResponse
    {
        return response()->json([
            'alertMessage' => AlertMessage::success(__('Your profile picture has been deleted successfully!'))->toArray(),
            'user' => UserResource::make($user),
        ]);
    }
}
