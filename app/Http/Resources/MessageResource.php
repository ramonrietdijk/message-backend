<?php

namespace App\Http\Resources;

use App\Models\Message;
use App\Models\User;
use Carbon\CarbonInterface;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin Message
 */
class MessageResource extends JsonResource
{
    /** @var array<int, string> */
    public $with = [
        'user',
    ];

    /**
     * @return array<string, mixed>
     */
    public function toArray($request): array
    {
        /** @var User $user */
        $user = $request->user();

        return [
            'id' => $this->id,
            'body' => $this->body,
            'created_at' => $this->created_at->format('d F Y \a\t H:i'),
            'profile' => ProfileResource::make($this->user),
            'age' => $this->created_at->diffForHumans(now(), CarbonInterface::DIFF_RELATIVE_TO_NOW),
            'like_count' => $this->likes()->count(),
            'has_liked' => $this->likes()->where('user_id', '=', $user->id)->count() > 0,
            'can_delete' => $user->can('delete', $this->resource),
        ];
    }
}
