<?php

namespace App\Http\Resources;

use App\Models\User;

class ProfileResource extends UserResource
{
    /**
     * @return array<string, mixed>
     */
    public function toArray($request): array
    {
        /** @var User $user */
        $user = $request->user();

        return [
            'username' => $this->username,
            'firstname' => $this->firstname,
            'lastname' => $this->lastname,
            'motto' => $this->motto,
            'picture' => $this->pictureUrl(),
            'is_public' => $this->is_public,
            'message_count' => $this->messages()->count(),
            'subscriber_count' => $this->subscribers()->count(),
            'subscription_count' => $this->subscriptions()->count(),
            'is_subscribed' => $this->subscribers()->where('user_id', '=', $user->id)->count() > 0,
            'can_subscribe' => $this->id !== $user->id,
        ];
    }
}
