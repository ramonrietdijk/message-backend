<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * @property string $current_password
 * @property string $password
 * @property string $password_confirmation
 */
class UpdatePasswordRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'current_password' => 'required|string|min:8|max:255',
            'password' => 'required|string|min:8|max:255',
            'password_confirmation' => 'required|string|min:8|max:255|same:password',
        ];
    }

    public function passedValidation(): void
    {
        if (Hash::check($this->current_password, $this->user()->password) === false) {
            throw ValidationException::withMessages(['current_password' => 'Your current password is incorrect']);
        }
    }
}
