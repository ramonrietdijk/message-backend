<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\BaseRequest;

/**
 * @property ?string $firstname
 * @property ?string $lastname
 * @property ?string $motto
 */
class UpdateGeneralRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'firstname' => 'string|max:255|nullable',
            'lastname' => 'string|max:255|nullable',
            'motto' => 'string|max:1000|nullable',
        ];
    }
}
