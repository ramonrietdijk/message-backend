<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\BaseRequest;

class UpdatePictureRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'picture' => [
                'required',
                'file',
                'image',
                'mimetypes:image/png,image/jpg,image/jpeg,image/gif,image/webp',
                'mimes:png,jpg,jpeg,gif,webp',
                'max:1024',
                'dimensions:max_width:4000,max_height:4000',
            ],
        ];
    }
}
