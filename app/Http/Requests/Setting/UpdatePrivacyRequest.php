<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\BaseRequest;

/**
 * @property bool $is_public
 */
class UpdatePrivacyRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'is_public' => 'required|bool',
        ];
    }
}
