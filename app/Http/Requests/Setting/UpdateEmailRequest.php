<?php

namespace App\Http\Requests\Setting;

use App\Http\Requests\BaseRequest;

/**
 * @property string $email
 */
class UpdateEmailRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'email' => 'required|string|email|unique:users|max:255',
        ];
    }
}
