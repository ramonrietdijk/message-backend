<?php

namespace App\Http\Requests\Subscription;

use App\Exceptions\AlertException;
use App\Http\Requests\BaseRequest;
use App\Models\User;

/**
 * @property User $profile
 */
class SubscriptionRequest extends BaseRequest
{
    protected function passedValidation(): void
    {
        if ($this->user()->id === $this->profile->id) {
            throw new AlertException(__('You can\'t subscribe to yourself'), 400);
        }
    }
}
