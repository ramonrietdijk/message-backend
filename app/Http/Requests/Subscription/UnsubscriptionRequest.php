<?php

namespace App\Http\Requests\Subscription;

use App\Http\Requests\BaseRequest;
use App\Models\User;

/**
 * @property User $profile
 */
class UnsubscriptionRequest extends BaseRequest
{
    //
}
