<?php

namespace App\Http\Requests\Like;

use App\Http\Requests\BaseRequest;
use App\Models\Message;

/**
 * @property Message $message
 */
class LikeMessageRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('view', $this->message);
    }
}
