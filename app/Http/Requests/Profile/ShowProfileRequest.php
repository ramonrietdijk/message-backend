<?php

namespace App\Http\Requests\Profile;

use App\Http\Requests\BaseRequest;
use App\Models\User;

/**
 * @property User $profile
 */
class ShowProfileRequest extends BaseRequest
{
    //
}
