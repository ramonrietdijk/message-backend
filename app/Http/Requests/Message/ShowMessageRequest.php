<?php

namespace App\Http\Requests\Message;

use App\Http\Requests\BaseRequest;
use App\Models\Message;

/**
 * @property Message $message
 */
class ShowMessageRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('view', $this->message);
    }
}
