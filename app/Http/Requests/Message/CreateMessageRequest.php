<?php

namespace App\Http\Requests\Message;

use App\Http\Requests\BaseRequest;
use App\Models\Message;

/**
 * @property string $body
 */
class CreateMessageRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('create', Message::class);
    }

    public function rules(): array
    {
        return [
            'body' => 'required|string|min:6|max:1000',
        ];
    }
}
