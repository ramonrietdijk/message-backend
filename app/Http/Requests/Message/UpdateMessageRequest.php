<?php

namespace App\Http\Requests\Message;

use App\Http\Requests\BaseRequest;
use App\Models\Message;

/**
 * @property string $body
 * @property Message $message
 */
class UpdateMessageRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('update', $this->message);
    }

    public function rules(): array
    {
        return [
            'body' => 'required|string|min:6|max:1000',
        ];
    }
}
