<?php

namespace App\Http\Requests\Message;

use App\Http\Requests\BaseRequest;
use App\Models\Message;

/**
 * @property Message $message
 */
class DeleteMessageRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('delete', $this->message);
    }
}
