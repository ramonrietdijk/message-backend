<?php

namespace App\Http\Requests\Feed;

use App\Http\Requests\BaseRequest;
use App\Models\User;

/**
 * @property User $profile
 */
class FetchUserFeedRequest extends BaseRequest
{
    public function authorize(): bool
    {
        return $this->user()->can('feed', $this->profile);
    }
}
