<?php

namespace App\Http\Middleware;

use App\Contracts\Auth\DecodesTokens;
use App\Exceptions\AlertException;
use Closure;
use Exception;
use Firebase\JWT\ExpiredException;
use Illuminate\Http\Request;

class TokenMiddleware
{
    public function __construct(
        protected DecodesTokens $contract
    ) {
    }

    /**
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $token = $request->bearerToken();

        if ($token === null) {
            throw new AlertException(__('Token not provided'), 401);
        }

        try {
            $user = $this->contract->decode($token);

            auth()->setUser($user);

            return $next($request);
        } catch (ExpiredException) {
            throw new AlertException(__('The provided token is expired'), 401);
        } catch (Exception) {
            throw new AlertException(__('An error occurred while decoding the token'), 401);
        }
    }
}
