<?php

namespace App\Http\Controllers;

use App\Contracts\Auth\AuthenticatesUsers;
use App\Contracts\Auth\CreatesUsers;
use App\Contracts\Auth\GeneratesTokens;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RefreshTokenRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\UserRequest;
use App\Http\Responses\Auth\LoginResponse;
use App\Http\Responses\Auth\RefreshTokenResponse;
use App\Http\Responses\Auth\RegisterResponse;
use App\Http\Responses\Auth\UserResponse;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    public function login(LoginRequest $request, LoginResponse $response, AuthenticatesUsers $contract, GeneratesTokens $token): JsonResponse
    {
        $user = $contract->authenticate($request->email, $request->password);

        return $response->make($user, $token->generate($user));
    }

    public function register(RegisterRequest $request, RegisterResponse $response, CreatesUsers $contract): JsonResponse
    {
        return $response->make(
            $contract->create(
                email: $request->email,
                username: $request->username,
                password: $request->password
            )
        );
    }

    public function refreshToken(RefreshTokenRequest $request, RefreshTokenResponse $response, GeneratesTokens $contract): JsonResponse
    {
        return $response->make($contract->generate($request->user()));
    }

    public function user(UserRequest $request, UserResponse $response): JsonResponse
    {
        return $response->make($request->user());
    }
}
