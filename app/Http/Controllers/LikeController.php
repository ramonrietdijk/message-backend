<?php

namespace App\Http\Controllers;

use App\Contracts\Like\LikesMessages;
use App\Http\Requests\Like\LikeMessageRequest;
use App\Http\Responses\Like\LikeMessageResponse;
use App\Models\Message;
use Illuminate\Http\JsonResponse;

class LikeController extends Controller
{
    public function message(LikeMessageRequest $request, LikeMessageResponse $response, LikesMessages $contract, Message $message): JsonResponse
    {
        return $response->make($contract->like($request->user(), $message));
    }
}
