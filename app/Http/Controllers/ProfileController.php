<?php

namespace App\Http\Controllers;

use App\Http\Requests\Profile\ShowProfileRequest;
use App\Http\Responses\Profile\ShowProfileResponse;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class ProfileController extends Controller
{
    public function show(ShowProfileRequest $request, ShowProfileResponse $response, User $profile): JsonResponse
    {
        return $response->make($profile);
    }
}
