<?php

namespace App\Http\Controllers;

use App\Contracts\Subscription\Subscribes;
use App\Contracts\Subscription\Unsubscribes;
use App\Http\Requests\Subscription\SubscriptionRequest;
use App\Http\Requests\Subscription\UnsubscriptionRequest;
use App\Http\Responses\Subscription\SubscriptionResponse;
use App\Http\Responses\Subscription\UnsubscriptionResponse;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class SubscriptionController extends Controller
{
    public function subscribe(SubscriptionRequest $request, SubscriptionResponse $response, Subscribes $contract, User $profile): JsonResponse
    {
        return $response->make($contract->subscribe($request->user(), $profile));
    }

    public function unsubscribe(UnsubscriptionRequest $request, UnsubscriptionResponse $response, Unsubscribes $contract, User $profile): JsonResponse
    {
        return $response->make($contract->unsubscribe($request->user(), $profile));
    }
}
