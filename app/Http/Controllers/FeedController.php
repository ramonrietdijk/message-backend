<?php

namespace App\Http\Controllers;

use App\Contracts\Feed\FetchesFeeds;
use App\Contracts\Feed\FetchesUserFeeds;
use App\Http\Requests\Feed\FetchFeedRequest;
use App\Http\Requests\Feed\FetchUserFeedRequest;
use App\Http\Responses\Feed\FetchFeedResponse;
use App\Http\Responses\Feed\FetchUserFeedResponse;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class FeedController extends Controller
{
    public function feed(FetchFeedRequest $request, FetchFeedResponse $response, FetchesFeeds $contract, int $limit, int $last): JsonResponse
    {
        return $response->make(
            $contract->fetch(
                user: $request->user(),
                limit: $limit,
                last: $last
            )
        );
    }

    public function user(FetchUserFeedRequest $request, FetchUserFeedResponse $response, FetchesUserFeeds $contract, User $profile, int $limit, int $last): JsonResponse
    {
        return $response->make(
            $contract->fetch(
                user: $profile,
                limit: $limit,
                last: $last
            )
        );
    }
}
