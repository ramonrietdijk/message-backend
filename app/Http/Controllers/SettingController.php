<?php

namespace App\Http\Controllers;

use App\Contracts\Setting\DeletesPictures;
use App\Contracts\Setting\UpdatesEmails;
use App\Contracts\Setting\UpdatesGeneral;
use App\Contracts\Setting\UpdatesPasswords;
use App\Contracts\Setting\UpdatesPictures;
use App\Contracts\Setting\UpdatesPrivacy;
use App\Http\Requests\Setting\DeletePictureRequest;
use App\Http\Requests\Setting\UpdateEmailRequest;
use App\Http\Requests\Setting\UpdateGeneralRequest;
use App\Http\Requests\Setting\UpdatePasswordRequest;
use App\Http\Requests\Setting\UpdatePictureRequest;
use App\Http\Requests\Setting\UpdatePrivacyRequest;
use App\Http\Responses\Setting\DeletePictureResponse;
use App\Http\Responses\Setting\UpdateEmailResponse;
use App\Http\Responses\Setting\UpdateGeneralResponse;
use App\Http\Responses\Setting\UpdatePasswordResponse;
use App\Http\Responses\Setting\UpdatePictureResponse;
use App\Http\Responses\Setting\UpdatePrivacyResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\UploadedFile;

class SettingController extends Controller
{
    public function updateGeneral(UpdateGeneralRequest $request, UpdateGeneralResponse $response, UpdatesGeneral $contract): JsonResponse
    {
        return $response->make(
            $contract->update(
                user: $request->user(),
                firstname: $request->firstname,
                lastname: $request->lastname,
                motto: $request->motto
            )
        );
    }

    public function updatePicture(UpdatePictureRequest $request, UpdatePictureResponse $response, UpdatesPictures $contract): JsonResponse
    {
        /** @var UploadedFile $picture */
        $picture = $request->file('picture');

        return $response->make($contract->update($request->user(), $picture));
    }

    public function deletePicture(DeletePictureRequest $request, DeletePictureResponse $response, DeletesPictures $contract): JsonResponse
    {
        return $response->make($contract->delete($request->user()));
    }

    public function updateEmail(UpdateEmailRequest $request, UpdateEmailResponse $response, UpdatesEmails $contract): JsonResponse
    {
        return $response->make($contract->update($request->user(), $request->email));
    }

    public function updatePassword(UpdatePasswordRequest $request, UpdatePasswordResponse $response, UpdatesPasswords $contract): JsonResponse
    {
        return $response->make($contract->update($request->user(), $request->password));
    }

    public function updatePrivacy(UpdatePrivacyRequest $request, UpdatePrivacyResponse $response, UpdatesPrivacy $contract): JsonResponse
    {
        return $response->make($contract->update($request->user(), $request->is_public));
    }
}
