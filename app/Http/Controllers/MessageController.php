<?php

namespace App\Http\Controllers;

use App\Contracts\Message\CreatesMessages;
use App\Contracts\Message\DeletesMessages;
use App\Contracts\Message\UpdatesMessages;
use App\Http\Requests\Message\CreateMessageRequest;
use App\Http\Requests\Message\DeleteMessageRequest;
use App\Http\Requests\Message\ShowMessageRequest;
use App\Http\Requests\Message\UpdateMessageRequest;
use App\Http\Responses\Message\CreateMessageResponse;
use App\Http\Responses\Message\DeleteMessageResponse;
use App\Http\Responses\Message\ShowMessageResponse;
use App\Http\Responses\Message\UpdateMessageResponse;
use App\Models\Message;
use Illuminate\Http\JsonResponse;

class MessageController extends Controller
{
    public function show(ShowMessageRequest $request, ShowMessageResponse $response, Message $message): JsonResponse
    {
        return $response->make($message);
    }

    public function create(CreateMessageRequest $request, CreateMessageResponse $response, CreatesMessages $contract): JsonResponse
    {
        return $response->make($contract->create($request->user(), $request->body));
    }

    public function update(UpdateMessageRequest $request, UpdateMessageResponse $response, UpdatesMessages $contract, Message $message): JsonResponse
    {
        return $response->make($contract->update($message, $request->body));
    }

    public function delete(DeleteMessageRequest $request, DeleteMessageResponse $response, DeletesMessages $contract, Message $message): JsonResponse
    {
        return $response->make($contract->delete($message));
    }
}
