<?php

namespace App\Contracts\Subscription;

use App\Models\User;

interface Subscribes
{
    public function subscribe(User $user, User $profile): User;
}
