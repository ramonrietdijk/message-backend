<?php

namespace App\Contracts\Subscription;

use App\Models\User;

interface Unsubscribes
{
    public function unsubscribe(User $user, User $profile): User;
}
