<?php

namespace App\Contracts\Like;

use App\Models\Message;
use App\Models\User;

interface LikesMessages
{
    public function like(User $user, Message $message): Message;
}
