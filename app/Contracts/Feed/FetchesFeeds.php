<?php

namespace App\Contracts\Feed;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Enumerable;

interface FetchesFeeds
{
    /**
     * @return Enumerable<int, Model>
     */
    public function fetch(User $user, int $limit, int $last): Enumerable;
}
