<?php

namespace App\Contracts\Message;

use App\Models\Message;

interface UpdatesMessages
{
    public function update(Message $message, string $body): Message;
}
