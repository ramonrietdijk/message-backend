<?php

namespace App\Contracts\Message;

use App\Models\Message;
use App\Models\User;

interface CreatesMessages
{
    public function create(User $user, string $body): Message;
}
