<?php

namespace App\Contracts\Message;

use App\Models\Message;

interface DeletesMessages
{
    public function delete(Message $message): bool;
}
