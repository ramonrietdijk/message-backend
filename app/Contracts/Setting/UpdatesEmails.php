<?php

namespace App\Contracts\Setting;

use App\Models\User;

interface UpdatesEmails
{
    public function update(User $user, string $email): User;
}
