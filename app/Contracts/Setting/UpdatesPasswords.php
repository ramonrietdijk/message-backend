<?php

namespace App\Contracts\Setting;

use App\Models\User;

interface UpdatesPasswords
{
    public function update(User $user, string $password): User;
}
