<?php

namespace App\Contracts\Setting;

use App\Models\User;

interface DeletesPictures
{
    public function delete(User $user): User;
}
