<?php

namespace App\Contracts\Setting;

use App\Models\User;

interface UpdatesGeneral
{
    public function update(User $user, ?string $firstname, ?string $lastname, ?string $motto): User;
}
