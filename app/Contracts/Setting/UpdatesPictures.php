<?php

namespace App\Contracts\Setting;

use App\Models\User;
use Illuminate\Http\UploadedFile;

interface UpdatesPictures
{
    public function update(User $user, UploadedFile $file): User;
}
