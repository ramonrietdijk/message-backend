<?php

namespace App\Contracts\Setting;

use App\Models\User;

interface UpdatesPrivacy
{
    public function update(User $user, bool $public): User;
}
