<?php

namespace App\Contracts\Auth;

use App\Models\User;

interface CreatesUsers
{
    public function create(string $email, string $username, string $password): User;
}
