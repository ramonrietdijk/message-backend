<?php

namespace App\Contracts\Auth;

use App\Models\User;

interface AuthenticatesUsers
{
    public function authenticate(string $email, string $password): User;
}
