<?php

namespace App\Contracts\Auth;

use App\Models\User;

interface GeneratesTokens
{
    public function generate(User $user): string;
}
