<?php

namespace App\Contracts\Auth;

use App\Models\User;

interface DecodesTokens
{
    public function decode(string $token): User;
}
