<?php

namespace App\Providers;

use App\Actions\Auth\AuthenticateUser;
use App\Actions\Auth\CreateUser;
use App\Actions\Auth\DecodeToken;
use App\Actions\Auth\GenerateToken;
use App\Actions\Feed\FetchFeed;
use App\Actions\Feed\FetchUserFeed;
use App\Actions\Like\LikeMessage;
use App\Actions\Message\CreateMessage;
use App\Actions\Message\DeleteMessage;
use App\Actions\Message\UpdateMessage;
use App\Actions\Setting\DeletePicture;
use App\Actions\Setting\UpdateEmail;
use App\Actions\Setting\UpdateGeneral;
use App\Actions\Setting\UpdatePassword;
use App\Actions\Setting\UpdatePicture;
use App\Actions\Setting\UpdatePrivacy;
use App\Actions\Subscription\Subscribe;
use App\Actions\Subscription\Unsubscribe;
use App\Services\JwtService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this
            ->registerActions()
            ->registerServices();
    }

    protected function registerActions(): static
    {
        // Authentication
        CreateUser::bind();
        AuthenticateUser::bind();
        GenerateToken::bind();
        DecodeToken::bind();

        // Settings
        UpdateGeneral::bind();
        UpdateEmail::bind();
        UpdatePassword::bind();
        UpdatePrivacy::bind();
        UpdatePicture::bind();
        DeletePicture::bind();

        // Message
        CreateMessage::bind();
        UpdateMessage::bind();
        DeleteMessage::bind();

        // Like
        LikeMessage::bind();

        // Subscription
        Subscribe::bind();
        Unsubscribe::bind();

        // Feed
        FetchFeed::bind();
        FetchUserFeed::bind();

        return $this;
    }

    protected function registerServices(): static
    {
        $this->app->singleton(JwtService::class, fn (): JwtService => new JwtService(
            secret: config('jwt.secret'),
            algorithm: config('jwt.algorithm'),
            expire: config('jwt.expire')
        ));

        return $this;
    }

    public function boot(): void
    {
        //
    }
}
