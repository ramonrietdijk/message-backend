<?php

namespace App\Concerns;

use Illuminate\Support\Facades\Storage;

/**
 * @property ?string $picture
 * @property ?int $picture_time
 */
trait HasPicture
{
    public function pictureUrl(): ?string
    {
        if ($this->picture === null) {
            return null;
        }

        $url = url(Storage::disk('pictures')->url($this->picture));

        if ($this->picture_time !== null) {
            $url .= '?t='.$this->picture_time;
        }

        return $url;
    }
}
