<?php

namespace App\Utilities;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\JsonResponse;

/**
 * @implements Arrayable<string, mixed>
 */
final class AlertMessage implements Arrayable
{
    public function __construct(
        protected string $type,
        protected string $message
    ) {
    }

    public static function make(string $type, string $message): static
    {
        return new self($type, $message);
    }

    public static function success(string $message): static
    {
        return static::make(__FUNCTION__, $message);
    }

    public static function danger(string $message): static
    {
        return static::make(__FUNCTION__, $message);
    }

    public static function warning(string $message): static
    {
        return static::make(__FUNCTION__, $message);
    }

    public static function info(string $message): static
    {
        return static::make(__FUNCTION__, $message);
    }

    /**
     * @param  array<string, mixed>  $headers
     */
    public function asJsonResponse(int $status = 200, array $headers = [], int $options = 0): JsonResponse
    {
        return response()->json(['alertMessage' => $this->toArray()], $status, $headers, $options);
    }

    public function toArray(): array
    {
        return [
            'type' => $this->type,
            'message' => $this->message,
        ];
    }
}
