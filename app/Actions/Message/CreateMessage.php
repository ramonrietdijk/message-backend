<?php

namespace App\Actions\Message;

use App\Contracts\Message\CreatesMessages;
use App\Models\Message;
use App\Models\User;

class CreateMessage implements CreatesMessages
{
    public function create(User $user, string $body): Message
    {
        $message = new Message;
        $message->user_id = $user->id;
        $message->body = $body;
        $message->save();

        return $message;
    }

    public static function bind(): void
    {
        app()->singleton(CreatesMessages::class, static::class);
    }
}
