<?php

namespace App\Actions\Message;

use App\Contracts\Message\DeletesMessages;
use App\Models\Message;

class DeleteMessage implements DeletesMessages
{
    public function delete(Message $message): bool
    {
        return $message->delete() ?? false;
    }

    public static function bind(): void
    {
        app()->singleton(DeletesMessages::class, static::class);
    }
}
