<?php

namespace App\Actions\Message;

use App\Contracts\Message\UpdatesMessages;
use App\Models\Message;

class UpdateMessage implements UpdatesMessages
{
    public function update(Message $message, string $body): Message
    {
        $message->body = $body;
        $message->save();

        return $message;
    }

    public static function bind(): void
    {
        app()->singleton(UpdatesMessages::class, static::class);
    }
}
