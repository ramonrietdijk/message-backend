<?php

namespace App\Actions\Auth;

use App\Contracts\Auth\DecodesTokens;
use App\Models\User;
use App\Services\JwtService;

class DecodeToken implements DecodesTokens
{
    public function __construct(
        protected JwtService $jwtService
    ) {
    }

    public function decode(string $token): User
    {
        return $this->jwtService->decode($token);
    }

    public static function bind(): void
    {
        app()->singleton(DecodesTokens::class, static::class);
    }
}
