<?php

namespace App\Actions\Auth;

use App\Contracts\Auth\CreatesUsers;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class CreateUser implements CreatesUsers
{
    public function create(string $email, string $username, string $password): User
    {
        $user = new User;
        $user->email = $email;
        $user->username = $username;
        $user->password = Hash::make($password);
        $user->save();

        return $user;
    }

    public static function bind(): void
    {
        app()->singleton(CreatesUsers::class, static::class);
    }
}
