<?php

namespace App\Actions\Auth;

use App\Contracts\Auth\GeneratesTokens;
use App\Models\User;
use App\Services\JwtService;

class GenerateToken implements GeneratesTokens
{
    public function __construct(
        protected JwtService $jwtService
    ) {
    }

    public function generate(User $user): string
    {
        return $this->jwtService->encode($user);
    }

    public static function bind(): void
    {
        app()->singleton(GeneratesTokens::class, static::class);
    }
}
