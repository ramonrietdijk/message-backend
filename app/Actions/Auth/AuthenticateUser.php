<?php

namespace App\Actions\Auth;

use App\Contracts\Auth\AuthenticatesUsers;
use App\Exceptions\AlertException;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AuthenticateUser implements AuthenticatesUsers
{
    public function authenticate(string $email, string $password): User
    {
        /** @var ?User $user */
        $user = User::query()
            ->where('email', '=', $email)
            ->first();

        if ($user === null || ! Hash::check($password, $user->password)) {
            throw new AlertException(__('Incorrect credentials'), 400);
        }

        return $user;
    }

    public static function bind(): void
    {
        app()->singleton(AuthenticatesUsers::class, static::class);
    }
}
