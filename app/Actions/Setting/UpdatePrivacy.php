<?php

namespace App\Actions\Setting;

use App\Contracts\Setting\UpdatesPrivacy;
use App\Models\User;

class UpdatePrivacy implements UpdatesPrivacy
{
    public function update(User $user, bool $public): User
    {
        $user->is_public = $public;
        $user->save();

        return $user;
    }

    public static function bind(): void
    {
        app()->singleton(UpdatesPrivacy::class, static::class);
    }
}
