<?php

namespace App\Actions\Setting;

use App\Contracts\Setting\UpdatesGeneral;
use App\Models\User;

class UpdateGeneral implements UpdatesGeneral
{
    public function update(User $user, ?string $firstname, ?string $lastname, ?string $motto): User
    {
        $user->firstname = $firstname ?? '';
        $user->lastname = $lastname ?? '';
        $user->motto = $motto ?? '';
        $user->save();

        return $user;
    }

    public static function bind(): void
    {
        app()->singleton(UpdatesGeneral::class, static::class);
    }
}
