<?php

namespace App\Actions\Setting;

use App\Contracts\Setting\DeletesPictures;
use App\Models\User;
use Illuminate\Support\Facades\Storage;

class DeletePicture implements DeletesPictures
{
    public function delete(User $user): User
    {
        if ($user->picture !== null) {
            Storage::disk('pictures')->delete($user->picture);
        }

        $user->picture = null;
        $user->picture_time = null;
        $user->save();

        return $user;
    }

    public static function bind(): void
    {
        app()->singleton(DeletesPictures::class, static::class);
    }
}
