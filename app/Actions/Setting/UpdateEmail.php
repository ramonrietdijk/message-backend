<?php

namespace App\Actions\Setting;

use App\Contracts\Setting\UpdatesEmails;
use App\Models\User;

class UpdateEmail implements UpdatesEmails
{
    public function update(User $user, string $email): User
    {
        $user->email = $email;
        $user->save();

        return $user;
    }

    public static function bind(): void
    {
        app()->singleton(UpdatesEmails::class, static::class);
    }
}
