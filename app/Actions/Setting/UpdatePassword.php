<?php

namespace App\Actions\Setting;

use App\Contracts\Setting\UpdatesPasswords;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UpdatePassword implements UpdatesPasswords
{
    public function update(User $user, string $password): User
    {
        $user->password = Hash::make($password);
        $user->save();

        return $user;
    }

    public static function bind(): void
    {
        app()->singleton(UpdatesPasswords::class, static::class);
    }
}
