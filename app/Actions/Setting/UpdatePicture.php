<?php

namespace App\Actions\Setting;

use App\Contracts\Setting\UpdatesPictures;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManager;

class UpdatePicture implements UpdatesPictures
{
    public function update(User $user, UploadedFile $file): User
    {
        $name = Str::random(40).'.'.$file->extension();
        $path = Storage::disk('pictures')->path($name);

        ImageManager::imagick()
            ->read($file)
            ->cover(250, 250)
            ->save($path);

        if ($user->picture !== null) {
            Storage::disk('pictures')->delete($user->picture);
        }

        $user->picture = $name;
        $user->picture_time = time();
        $user->save();

        return $user;
    }

    public static function bind(): void
    {
        app()->singleton(UpdatesPictures::class, static::class);
    }
}
