<?php

namespace App\Actions\Subscription;

use App\Contracts\Subscription\Subscribes;
use App\Exceptions\AlertException;
use App\Models\Subscription;
use App\Models\User;

class Subscribe implements Subscribes
{
    public function subscribe(User $user, User $profile): User
    {
        $subscribed = $user->subscriptions()->where('related_id', '=', $profile->id)->count() > 0;

        if ($subscribed) {
            throw new AlertException(__('You\'re already subscribed to this user'), 400);
        }

        Subscription::query()->create([
            'user_id' => $user->id,
            'related_id' => $profile->id,
        ]);

        $user->load('subscriptions');

        return $profile->load('subscribers');
    }

    public static function bind(): void
    {
        app()->singleton(Subscribes::class, static::class);
    }
}
