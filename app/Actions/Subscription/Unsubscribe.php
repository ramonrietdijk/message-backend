<?php

namespace App\Actions\Subscription;

use App\Contracts\Subscription\Unsubscribes;
use App\Exceptions\AlertException;
use App\Models\Subscription;
use App\Models\User;

class Unsubscribe implements Unsubscribes
{
    public function unsubscribe(User $user, User $profile): User
    {
        $subscribed = $user->subscriptions()->where('related_id', '=', $profile->id)->count() > 0;

        if (! $subscribed) {
            throw new AlertException(__('You\'re not subscribed to this user'), 400);
        }

        Subscription::query()
            ->where('user_id', '=', $user->id)
            ->where('related_id', '=', $profile->id)
            ->delete();

        $user->load('subscriptions');

        return $profile->load('subscribers');
    }

    public static function bind(): void
    {
        app()->singleton(Unsubscribes::class, static::class);
    }
}
