<?php

namespace App\Actions\Like;

use App\Contracts\Like\LikesMessages;
use App\Models\Message;
use App\Models\User;

class LikeMessage implements LikesMessages
{
    public function like(User $user, Message $message): Message
    {
        $liked = $message->likes()->where('user_id', '=', $user->id)->count() > 0;

        if ($liked) {
            $message->likes()->where('user_id', '=', $user->id)->delete();
        } else {
            $message->likes()->create(['user_id' => $user->id]);
        }

        return $message->load('likes');
    }

    public static function bind(): void
    {
        app()->singleton(LikesMessages::class, static::class);
    }
}
