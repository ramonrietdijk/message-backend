<?php

namespace App\Actions\Feed;

use App\Contracts\Feed\FetchesFeeds;
use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Enumerable;

class FetchFeed implements FetchesFeeds
{
    public const MAX_FEED_SIZE = 50;

    /**
     * @return Collection<int, Model>
     */
    public function fetch(User $user, int $limit, int $last): Enumerable
    {
        $feedLimit = min(static::MAX_FEED_SIZE, max(1, $limit));

        return Message::query()
            ->whereHas('user', function (Builder $builder) use ($user): void {
                $builder
                    ->where('users.id', '=', $user->id)
                    ->orWhere(function (Builder $builder) use ($user): void {
                        $builder
                            ->where('users.is_public', '=', 1)
                            ->whereHas('subscribers', function (Builder $builder) use ($user): void {
                                $builder->where('subscriptions.user_id', '=', $user->id);
                            });
                    });
            })
            ->when($last > 0, fn (Builder $builder): Builder => $builder->where('messages.id', '<', $last))
            ->orderBy('messages.id', 'desc')
            ->limit($feedLimit)
            ->get();
    }

    public static function bind(): void
    {
        app()->singleton(FetchesFeeds::class, static::class);
    }
}
