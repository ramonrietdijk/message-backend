<?php

namespace Database\Factories;

use App\Models\Like;
use App\Models\Message;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends Factory<Like>
 */
class LikeFactory extends Factory
{
    /**
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'likeable_type' => Message::class,
            'likeable_id' => Message::factory(),
            'user_id' => User::factory(),
        ];
    }
}
