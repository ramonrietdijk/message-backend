<?php

namespace Tests\Actions\Subscription;

use App\Actions\Subscription\Unsubscribe;
use App\Exceptions\AlertException;
use App\Models\Subscription;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UnsubscribeTest extends TestCase
{
    #[Test]
    public function it_can_unsubscribe(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var User $profile */
        $profile = User::factory()->create();

        Subscription::factory()
            ->for($user, 'user')
            ->for($profile, 'related')
            ->create();

        $this->assertCount(1, $user->subscriptions);
        $this->assertCount(1, $profile->subscribers);

        /** @var Unsubscribe $action */
        $action = app(Unsubscribe::class);
        $action->unsubscribe($user, $profile);

        $this->assertCount(0, $user->subscriptions);
        $this->assertCount(0, $profile->subscribers);
    }

    #[Test]
    public function it_checks_if_the_user_has_not_subscribed(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var User $profile */
        $profile = User::factory()->create();

        $this->expectException(AlertException::class);

        /** @var Unsubscribe $action */
        $action = app(Unsubscribe::class);
        $action->unsubscribe($user, $profile);
    }
}
