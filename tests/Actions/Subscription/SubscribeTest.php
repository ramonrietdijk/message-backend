<?php

namespace Tests\Actions\Subscription;

use App\Actions\Subscription\Subscribe;
use App\Exceptions\AlertException;
use App\Models\Subscription;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class SubscribeTest extends TestCase
{
    #[Test]
    public function it_can_subscribe(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var User $profile */
        $profile = User::factory()->create();

        $this->assertCount(0, $user->subscriptions);
        $this->assertCount(0, $profile->subscribers);

        /** @var Subscribe $action */
        $action = app(Subscribe::class);
        $action->subscribe($user, $profile);

        $this->assertCount(1, $user->subscriptions);
        $this->assertCount(1, $profile->subscribers);
    }

    #[Test]
    public function it_checks_if_the_user_has_already_subscribed(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var User $profile */
        $profile = User::factory()->create();

        Subscription::factory()
            ->for($user, 'user')
            ->for($profile, 'related')
            ->create();

        $this->expectException(AlertException::class);

        /** @var Subscribe $action */
        $action = app(Subscribe::class);
        $action->subscribe($user, $profile);
    }
}
