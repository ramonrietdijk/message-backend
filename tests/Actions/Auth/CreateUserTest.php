<?php

namespace Tests\Actions\Auth;

use App\Actions\Auth\CreateUser;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class CreateUserTest extends TestCase
{
    #[Test]
    public function it_creates_users(): void
    {
        /** @var CreateUser $action */
        $action = app(CreateUser::class);

        $user = $action->create(
            email: 'john.doe@example.com',
            username: 'john.doe',
            password: 'password'
        );

        $this->assertModelExists($user);
    }
}
