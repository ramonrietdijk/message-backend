<?php

namespace Tests\Actions\Auth;

use App\Actions\Auth\GenerateToken;
use App\Models\User;
use App\Services\JwtService;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class GenerateTokenTest extends TestCase
{
    #[Test]
    public function it_generates_tokens(): void
    {
        $this->mock(JwtService::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('encode')
                ->once()
                ->andReturn('::token::');
        });

        /** @var User $user */
        $user = User::factory()->create();

        /** @var GenerateToken $action */
        $action = app(GenerateToken::class);

        $token = $action->generate($user);

        $this->assertEquals('::token::', $token);
    }
}
