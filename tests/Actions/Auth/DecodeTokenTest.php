<?php

namespace Tests\Actions\Auth;

use App\Actions\Auth\DecodeToken;
use App\Models\User;
use App\Services\JwtService;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class DecodeTokenTest extends TestCase
{
    #[Test]
    public function it_decodes_tokens(): void
    {
        $this->mock(JwtService::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('decode')
                ->with('::token::')
                ->once()
                ->andReturn(User::factory()->create());
        });

        /** @var DecodeToken $action */
        $action = app(DecodeToken::class);

        $user = $action->decode('::token::');

        $this->assertModelExists($user);
    }
}
