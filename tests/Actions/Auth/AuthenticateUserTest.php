<?php

namespace Tests\Actions\Auth;

use App\Actions\Auth\AuthenticateUser;
use App\Exceptions\AlertException;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class AuthenticateUserTest extends TestCase
{
    #[Test]
    public function it_authenticates_users(): void
    {
        /** @var User $user */
        $user = User::factory()->create(['email' => 'john.doe@example.com']);

        /** @var AuthenticateUser $action */
        $action = app(AuthenticateUser::class);

        $authenticated = $action->authenticate('john.doe@example.com', 'password');

        $this->assertEquals($user->id, $authenticated->id);
    }

    #[Test]
    public function it_throws_exceptions_when_the_user_does_not_exist(): void
    {
        $this->expectException(AlertException::class);

        /** @var AuthenticateUser $action */
        $action = app(AuthenticateUser::class);
        $action->authenticate('john.doe@example.com', 'password');
    }

    #[Test]
    public function it_throws_exceptions_when_an_invalid_password_is_used(): void
    {
        User::factory()->create(['email' => 'john.doe@example.com']);

        $this->expectException(AlertException::class);

        /** @var AuthenticateUser $action */
        $action = app(AuthenticateUser::class);
        $action->authenticate('john.doe@example.com', 'incorrect');
    }
}
