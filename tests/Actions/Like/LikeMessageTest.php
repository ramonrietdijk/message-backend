<?php

namespace Tests\Actions\Like;

use App\Actions\Like\LikeMessage;
use App\Models\Message;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class LikeMessageTest extends TestCase
{
    #[Test]
    public function it_can_like_messages(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Message $message */
        $message = Message::factory()->for($user)->create();

        $this->assertCount(0, $message->likes);

        /** @var LikeMessage $action */
        $action = app(LikeMessage::class);
        $action->like($user, $message);

        $this->assertCount(1, $message->likes);

        $action->like($user, $message);

        $this->assertCount(0, $message->likes);
    }
}
