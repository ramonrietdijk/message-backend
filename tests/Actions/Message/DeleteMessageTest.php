<?php

namespace Tests\Actions\Message;

use App\Actions\Message\DeleteMessage;
use App\Models\Message;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class DeleteMessageTest extends TestCase
{
    #[Test]
    public function it_can_delete_messages(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Message $message */
        $message = Message::factory()->for($user)->create();

        /** @var DeleteMessage $action */
        $action = app(DeleteMessage::class);
        $action->delete($message);

        $this->assertModelMissing($message);
    }
}
