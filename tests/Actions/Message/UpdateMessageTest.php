<?php

namespace Tests\Actions\Message;

use App\Actions\Message\UpdateMessage;
use App\Models\Message;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UpdateMessageTest extends TestCase
{
    #[Test]
    public function it_can_update_messages(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Message $message */
        $message = Message::factory()->for($user)->create();

        /** @var UpdateMessage $action */
        $action = app(UpdateMessage::class);

        $updated = $action->update($message, 'body');

        $this->assertEquals('body', $updated->body);
    }
}
