<?php

namespace Tests\Actions\Message;

use App\Actions\Message\CreateMessage;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class CreateMessageTest extends TestCase
{
    #[Test]
    public function it_can_create_messages(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var CreateMessage $action */
        $action = app(CreateMessage::class);

        $message = $action->create($user, '::body::');

        $this->assertModelExists($message);
    }
}
