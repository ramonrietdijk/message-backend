<?php

namespace Tests\Actions\Setting;

use App\Actions\Setting\UpdateGeneral;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UpdateGeneralTest extends TestCase
{
    #[Test]
    public function it_can_update_general(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var UpdateGeneral $action */
        $action = app(UpdateGeneral::class);
        $action->update($user, 'John', 'Doe', 'Lorem ipsum');

        $this->assertEquals('John', $user->firstname);
        $this->assertEquals('Doe', $user->lastname);
        $this->assertEquals('Lorem ipsum', $user->motto);
    }
}
