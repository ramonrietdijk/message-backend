<?php

namespace Tests\Actions\Setting;

use App\Actions\Setting\UpdatePicture;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UpdatePictureTest extends TestCase
{
    #[Test]
    public function it_can_update_pictures(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        Storage::fake('pictures');

        $file = UploadedFile::fake()->image('image.png');

        /** @var UpdatePicture $action */
        $action = app(UpdatePicture::class);
        $action->update($user, $file);

        $this->assertNotNull($user->picture);
        $this->assertNotNull($user->picture_time);

        $match = preg_match('/.+?t=\d+/', $user->pictureUrl());

        $this->assertTrue($match !== false && $match > 0);

        $picture = $user->picture;

        Storage::disk('pictures')->assertExists($picture);

        $action->update($user, UploadedFile::fake()->image('image.png'));

        Storage::disk('pictures')->assertMissing($picture);
    }
}
