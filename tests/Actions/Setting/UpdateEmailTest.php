<?php

namespace Tests\Actions\Setting;

use App\Actions\Setting\UpdateEmail;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UpdateEmailTest extends TestCase
{
    #[Test]
    public function it_can_update_emails(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var UpdateEmail $action */
        $action = app(UpdateEmail::class);
        $action->update($user, 'john.doe@example.com');

        $this->assertEquals('john.doe@example.com', $user->email);
    }
}
