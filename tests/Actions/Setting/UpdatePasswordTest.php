<?php

namespace Tests\Actions\Setting;

use App\Actions\Setting\UpdatePassword;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UpdatePasswordTest extends TestCase
{
    #[Test]
    public function it_can_update_passwords(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var UpdatePassword $action */
        $action = app(UpdatePassword::class);
        $action->update($user, 'secret');

        $this->assertTrue(Hash::check('secret', $user->password));
    }
}
