<?php

namespace Tests\Actions\Setting;

use App\Actions\Setting\UpdatePrivacy;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UpdatePrivacyTest extends TestCase
{
    #[Test]
    public function it_can_update_privacy(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var UpdatePrivacy $action */
        $action = app(UpdatePrivacy::class);
        $action->update($user, false);

        $this->assertFalse($user->is_public);
    }
}
