<?php

namespace Tests\Actions\Setting;

use App\Actions\Setting\DeletePicture;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class DeletePictureTest extends TestCase
{
    #[Test]
    public function it_can_delete_pictures(): void
    {
        /** @var User $user */
        $user = User::factory()->create([
            'picture' => 'picture.png',
            'picture_time' => time(),
        ]);

        Storage::fake('pictures');

        UploadedFile::fake()->image('image.png')->store('', [
            'disk' => 'pictures',
        ]);

        /** @var DeletePicture $action */
        $action = app(DeletePicture::class);
        $action->delete($user);

        $this->assertNull($user->picture);
        $this->assertNull($user->picture_time);

        Storage::disk('pictures')->assertMissing('picture.png');
    }
}
