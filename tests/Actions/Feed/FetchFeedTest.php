<?php

namespace Tests\Actions\Feed;

use App\Actions\Feed\FetchFeed;
use App\Models\Message;
use App\Models\Subscription;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class FetchFeedTest extends TestCase
{
    #[Test]
    public function it_can_fetch_feeds(): void
    {
        /** @var User $user */
        $user = User::factory()->has(Message::factory())->create();

        $profile1 = User::factory()->has(Message::factory()->count(5))->create();
        $profile2 = User::factory()->has(Message::factory()->count(5))->create(['is_public' => false]);

        Subscription::factory()->for($user, 'user')->for($profile1, 'related')->create();
        Subscription::factory()->for($user, 'user')->for($profile2, 'related')->create();

        /** @var FetchFeed $action */
        $action = app(FetchFeed::class);

        $feed = $action->fetch($user, 25, 0);

        $this->assertCount(6, $feed);
    }
}
