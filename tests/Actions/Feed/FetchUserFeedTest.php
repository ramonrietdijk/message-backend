<?php

namespace Tests\Actions\Feed;

use App\Actions\Feed\FetchUserFeed;
use App\Models\Message;
use App\Models\User;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class FetchUserFeedTest extends TestCase
{
    #[Test]
    public function it_can_fetch_user_feeds(): void
    {
        /** @var User $user */
        $user = User::factory()->has(Message::factory()->count(5))->create();

        User::factory()->has(Message::factory()->count(5))->create();

        /** @var FetchUserFeed $action */
        $action = app(FetchUserFeed::class);

        $feed = $action->fetch($user, 25, 0);

        $this->assertCount(5, $feed);
    }
}
