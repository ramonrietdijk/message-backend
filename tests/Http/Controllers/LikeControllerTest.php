<?php

namespace Tests\Http\Controllers;

use App\Contracts\Like\LikesMessages;
use App\Http\Middleware\TokenMiddleware;
use App\Models\Message;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class LikeControllerTest extends TestCase
{
    #[Test]
    public function it_can_like_messages(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Message $message */
        $message = Message::factory()->for($user)->create();

        Auth::setUser($user);

        $this->mock(LikesMessages::class, function (MockInterface $mock) use ($message): void {
            $mock
                ->shouldReceive('like')
                ->once()
                ->andReturn($message);
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->post('api/v1/messages/'.$message->id.'/like')
            ->assertSuccessful()
            ->assertJsonStructure(['message']);
    }
}
