<?php

namespace Tests\Http\Controllers;

use App\Contracts\Feed\FetchesFeeds;
use App\Contracts\Feed\FetchesUserFeeds;
use App\Http\Middleware\TokenMiddleware;
use App\Models\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class FeedControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        /** @var User $user */
        $user = User::factory()->create();

        Auth::setUser($user);
    }

    #[Test]
    public function it_can_fetch_feeds(): void
    {
        $this->mock(FetchesFeeds::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('fetch')
                ->once()
                ->andReturn(new Collection());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->get('api/v1/messages/feed/25/0')
            ->assertSuccessful()
            ->assertJsonStructure(['messages']);
    }

    #[Test]
    public function it_can_fetch_user_feeds(): void
    {
        /** @var User $profile */
        $profile = User::factory()->create();

        $this->mock(FetchesUserFeeds::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('fetch')
                ->once()
                ->andReturn(new Collection());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->get('api/v1/messages/user/'.$profile->username.'/25/0')
            ->assertSuccessful()
            ->assertJsonStructure(['messages']);
    }

    #[Test]
    public function it_can_not_fetch_private_user_feeds(): void
    {
        /** @var User $profile */
        $profile = User::factory()->create([
            'is_public' => false,
        ]);

        $this->mock(FetchesUserFeeds::class);

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->get('api/v1/messages/user/'.$profile->username.'/25/0')
            ->assertForbidden();
    }
}
