<?php

namespace Tests\Http\Controllers;

use App\Contracts\Setting\DeletesPictures;
use App\Contracts\Setting\UpdatesEmails;
use App\Contracts\Setting\UpdatesGeneral;
use App\Contracts\Setting\UpdatesPasswords;
use App\Contracts\Setting\UpdatesPictures;
use App\Contracts\Setting\UpdatesPrivacy;
use App\Http\Middleware\TokenMiddleware;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class SettingControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        /** @var User $user */
        $user = User::factory()->create();

        Auth::setUser($user);
    }

    #[Test]
    public function it_can_update_general(): void
    {
        $this->mock(UpdatesGeneral::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('update')
                ->with(Auth::user(), 'John', 'Doe', 'Lorem ipsum')
                ->once()
                ->andReturn(Auth::user());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->patch('api/v1/settings/general', [
                'firstname' => 'John',
                'lastname' => 'Doe',
                'motto' => 'Lorem ipsum',
            ])
            ->assertSuccessful()
            ->assertJsonStructure(['user', 'alertMessage']);
    }

    #[Test]
    public function it_can_update_pictures(): void
    {
        $picture = UploadedFile::fake()->image('image.png');

        $this->mock(UpdatesPictures::class, function (MockInterface $mock) use ($picture): void {
            $mock
                ->shouldReceive('update')
                ->with(Auth::user(), $picture)
                ->once()
                ->andReturn(Auth::user());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->post('api/v1/settings/picture', ['picture' => $picture])
            ->assertSuccessful()
            ->assertJsonStructure(['user', 'alertMessage']);
    }

    #[Test]
    public function it_can_delete_pictures(): void
    {
        $this->mock(DeletesPictures::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('delete')
                ->with(Auth::user())
                ->once()
                ->andReturn(Auth::user());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->delete('api/v1/settings/picture')
            ->assertSuccessful()
            ->assertJsonStructure(['user', 'alertMessage']);
    }

    #[Test]
    public function it_can_update_emails(): void
    {
        $this->mock(UpdatesEmails::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('update')
                ->with(Auth::user(), 'john.doe@example.com')
                ->once()
                ->andReturn(Auth::user());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->patch('api/v1/settings/email', ['email' => 'john.doe@example.com'])
            ->assertSuccessful()
            ->assertJsonStructure(['user', 'alertMessage']);
    }

    #[Test]
    public function it_can_update_passwords(): void
    {
        $this->mock(UpdatesPasswords::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('update')
                ->with(Auth::user(), 'password')
                ->once()
                ->andReturn(Auth::user());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->patch('api/v1/settings/password', [
                'current_password' => 'password',
                'password' => 'password',
                'password_confirmation' => 'password',
            ])
            ->assertSuccessful()
            ->assertJsonStructure(['user', 'alertMessage']);
    }

    #[Test]
    public function it_can_verify_the_current_password(): void
    {
        $this->mock(UpdatesPasswords::class);

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->patch('api/v1/settings/password', [
                'current_password' => 'incorrect',
                'password' => 'password',
                'password_confirmation' => 'password',
            ])->assertInvalid('current_password');
    }

    #[Test]
    public function it_can_update_privacy(): void
    {
        $this->mock(UpdatesPrivacy::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('update')
                ->with(Auth::user(), true)
                ->once()
                ->andReturn(Auth::user());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->patch('api/v1/settings/privacy', ['is_public' => true])
            ->assertSuccessful()
            ->assertJsonStructure(['user', 'alertMessage']);
    }
}
