<?php

namespace Tests\Http\Controllers;

use App\Http\Middleware\TokenMiddleware;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class ProfileControllerTest extends TestCase
{
    #[Test]
    public function it_can_get_user_profiles(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        Auth::setUser($user);

        /** @var User $profile */
        $profile = User::factory()->create();

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->get('api/v1/profile/'.$profile->username)
            ->assertSuccessful()
            ->assertJsonStructure(['profile']);
    }
}
