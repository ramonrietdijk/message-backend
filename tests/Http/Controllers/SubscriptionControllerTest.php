<?php

namespace Tests\Http\Controllers;

use App\Contracts\Subscription\Subscribes;
use App\Contracts\Subscription\Unsubscribes;
use App\Http\Middleware\TokenMiddleware;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class SubscriptionControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        /** @var User $user */
        $user = User::factory()->create();

        Auth::setUser($user);
    }

    #[Test]
    public function it_can_subscribe(): void
    {
        /** @var User $profile */
        $profile = User::factory()->create();

        $this->mock(Subscribes::class, function (MockInterface $mock) use ($profile): void {
            $mock
                ->shouldReceive('subscribe')
                ->once()
                ->andReturn($profile);
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->post('api/v1/profile/'.$profile->username.'/subscribe')
            ->assertSuccessful()
            ->assertJsonStructure(['profile']);
    }

    #[Test]
    public function it_can_not_subscribe_to_itself(): void
    {
        /** @var User $user */
        $user = Auth::user();

        $this->mock(Subscribes::class);

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->post('api/v1/profile/'.$user->username.'/subscribe')
            ->assertStatus(400);
    }

    #[Test]
    public function it_can_unsubscribe(): void
    {
        /** @var User $profile */
        $profile = User::factory()->create();

        $this->mock(Unsubscribes::class, function (MockInterface $mock) use ($profile): void {
            $mock
                ->shouldReceive('unsubscribe')
                ->once()
                ->andReturn($profile);
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->post('api/v1/profile/'.$profile->username.'/unsubscribe')
            ->assertSuccessful()
            ->assertJsonStructure(['profile']);
    }
}
