<?php

namespace Tests\Http\Controllers;

use App\Contracts\Message\CreatesMessages;
use App\Contracts\Message\DeletesMessages;
use App\Contracts\Message\UpdatesMessages;
use App\Http\Middleware\TokenMiddleware;
use App\Models\Message;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class MessageControllerTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        /** @var User $user */
        $user = User::factory()->create();

        Auth::setUser($user);
    }

    #[Test]
    public function it_can_show_messages(): void
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var Message $message */
        $message = Message::factory()->for($user)->create();

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->get('api/v1/messages/'.$message->id)
            ->assertSuccessful()
            ->assertJsonStructure(['message']);
    }

    #[Test]
    public function it_can_create_messages(): void
    {
        /** @var User $user */
        $user = Auth::user();

        $this->mock(CreatesMessages::class, function (MockInterface $mock) use ($user): void {
            $mock
                ->shouldReceive('create')
                ->once()
                ->andReturn(Message::factory()->for($user)->create());
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->post('api/v1/messages', ['body' => 'Lorem ipsum'])
            ->assertSuccessful()
            ->assertJsonStructure(['message']);
    }

    #[Test]
    public function it_can_update_messages(): void
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var Message $message */
        $message = Message::factory()->for($user)->create();

        $this->mock(UpdatesMessages::class, function (MockInterface $mock) use ($message): void {
            $mock
                ->shouldReceive('update')
                ->once()
                ->andReturn($message);
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->patch('api/v1/messages/'.$message->id, ['body' => 'Lorem ipsum'])
            ->assertSuccessful()
            ->assertJsonStructure(['message']);
    }

    #[Test]
    public function it_can_delete_messages(): void
    {
        /** @var User $user */
        $user = Auth::user();

        /** @var Message $message */
        $message = Message::factory()->for($user)->create();

        $this->mock(DeletesMessages::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('delete')
                ->once()
                ->andReturn();
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->delete('api/v1/messages/'.$message->id)
            ->assertSuccessful();
    }
}
