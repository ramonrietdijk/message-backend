<?php

namespace Tests\Http\Controllers;

use App\Contracts\Auth\AuthenticatesUsers;
use App\Contracts\Auth\CreatesUsers;
use App\Contracts\Auth\GeneratesTokens;
use App\Http\Middleware\TokenMiddleware;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    #[Test]
    public function it_authenticates_users(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        $this->mock(AuthenticatesUsers::class, function (MockInterface $mock) use ($user): void {
            $mock
                ->shouldReceive('authenticate')
                ->with('john.doe@example.com', 'password')
                ->once()
                ->andReturn($user);
        });

        $this->mock(GeneratesTokens::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('generate')
                ->once()
                ->andReturn('::token::');
        });

        $this->post('api/v1/auth/login', [
            'email' => 'john.doe@example.com',
            'password' => 'password',
        ])
            ->assertSuccessful()
            ->assertJsonStructure(['user', 'token']);
    }

    #[Test]
    public function it_creates_users(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        $this->mock(CreatesUsers::class, function (MockInterface $mock) use ($user): void {
            $mock
                ->shouldReceive('create')
                ->with('john.doe@example.com', 'john_doe', 'password')
                ->once()
                ->andReturn($user);
        });

        $this->post('api/v1/auth/register', [
            'email' => 'john.doe@example.com',
            'username' => 'john_doe',
            'password' => 'password',
            'password_confirmation' => 'password',
        ])
            ->assertSuccessful()
            ->assertJsonStructure(['alertMessage']);
    }

    #[Test]
    public function it_can_refresh_tokens(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        Auth::setUser($user);

        $this->mock(GeneratesTokens::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('generate')
                ->once()
                ->andReturn('::token::');
        });

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->post('api/v1/auth/refresh')
            ->assertSuccessful()
            ->assertJsonStructure(['token']);
    }

    #[Test]
    public function it_can_get_user_information(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        Auth::setUser($user);

        $this
            ->withoutMiddleware(TokenMiddleware::class)
            ->get('api/v1/auth/user')
            ->assertSuccessful()
            ->assertJsonStructure(['user']);
    }
}
