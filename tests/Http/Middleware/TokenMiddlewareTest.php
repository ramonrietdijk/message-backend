<?php

namespace Tests\Http\Middleware;

use App\Contracts\Auth\DecodesTokens;
use App\Contracts\Auth\GeneratesTokens;
use App\Exceptions\AlertException;
use App\Http\Middleware\TokenMiddleware;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Mockery\MockInterface;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class TokenMiddlewareTest extends TestCase
{
    #[Test]
    public function it_can_decode_tokens(): void
    {
        $this->mock(DecodesTokens::class, function (MockInterface $mock): void {
            $mock
                ->shouldReceive('decode')
                ->with('::token::')
                ->once()
                ->andReturn(User::factory()->create());
        });

        /** @var TokenMiddleware $middleware */
        $middleware = app(TokenMiddleware::class);

        /** @var Request $request */
        $request = app(Request::class);
        $request->headers->set('Authorization', 'Bearer ::token::');

        /** @var Response $response */
        $response = $middleware->handle($request, fn (): Response => response()->json());

        $this->assertTrue($response->isOk());
        $this->assertAuthenticated();
    }

    #[Test]
    public function it_must_provide_tokens(): void
    {
        $this->mock(DecodesTokens::class);

        /** @var TokenMiddleware $middleware */
        $middleware = app(TokenMiddleware::class);

        /** @var Request $request */
        $request = app(Request::class);

        $this->expectException(AlertException::class);

        $middleware->handle($request, fn () => $this->fail('Passed middleware'));
    }

    #[Test]
    public function it_can_check_expired_tokens(): void
    {
        Carbon::setTestNow(now()->subDay());

        /** @var User $user */
        $user = User::factory()->create();

        /** @var GeneratesTokens $token */
        $token = app(GeneratesTokens::class);

        /** @var TokenMiddleware $middleware */
        $middleware = app(TokenMiddleware::class);

        /** @var Request $request */
        $request = app(Request::class);
        $request->headers->set('Authorization', 'Bearer '.$token->generate($user));

        $this->expectException(AlertException::class);

        $middleware->handle($request, fn () => $this->fail('Passed middleware'));
    }

    #[Test]
    public function it_can_throw_exceptions(): void
    {
        /** @var TokenMiddleware $middleware */
        $middleware = app(TokenMiddleware::class);

        /** @var Request $request */
        $request = app(Request::class);
        $request->headers->set('Authorization', 'Bearer ::token::');

        $this->expectException(AlertException::class);

        $middleware->handle($request, fn () => $this->fail('Passed middleware'));
    }
}
