<?php

namespace Tests\Http\Resources;

use App\Http\Resources\ProfileResource;
use App\Models\User;
use Illuminate\Http\Request;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class ProfileResourceTest extends TestCase
{
    #[Test]
    public function it_can_format_profiles(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        $resource = ProfileResource::make($user);

        /** @var Request $request */
        $request = app(Request::class);
        $request->setUserResolver(fn (): User => $user);

        $data = $resource->toArray($request);

        $this->assertEquals([
            'username',
            'firstname',
            'lastname',
            'motto',
            'picture',
            'is_public',
            'message_count',
            'subscriber_count',
            'subscription_count',
            'is_subscribed',
            'can_subscribe',
        ], array_keys($data));
    }
}
