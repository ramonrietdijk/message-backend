<?php

namespace Tests\Http\Resources;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class UserResourceTest extends TestCase
{
    #[Test]
    public function it_can_format_users(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        $resource = UserResource::make($user);

        /** @var Request $request */
        $request = app(Request::class);

        $this->assertEquals([
            'username' => $user->username,
            'email' => $user->email,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'motto' => $user->motto,
            'picture' => $user->picture,
            'is_public' => $user->is_public,
        ], $resource->toArray($request));
    }
}
