<?php

namespace Tests\Http\Resources;

use App\Http\Resources\MessageResource;
use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class MessageResourceTest extends TestCase
{
    #[Test]
    public function it_can_format_messages(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var Message $message */
        $message = Message::factory()->create();

        $resource = MessageResource::make($message);

        /** @var Request $request */
        $request = app(Request::class);
        $request->setUserResolver(fn (): User => $user);

        $data = $resource->toArray($request);

        $this->assertEquals([
            'id',
            'body',
            'created_at',
            'profile',
            'age',
            'like_count',
            'has_liked',
            'can_delete',
        ], array_keys($data));
    }
}
