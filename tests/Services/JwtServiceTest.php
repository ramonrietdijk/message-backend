<?php

namespace Tests\Services;

use App\Models\User;
use App\Services\JwtService;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class JwtServiceTest extends TestCase
{
    #[Test]
    public function it_can_encode_and_decode_tokens(): void
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var JwtService $jwtService */
        $jwtService = app(JwtService::class);

        $token = $jwtService->encode($user);

        $decodedUser = $jwtService->decode($token);

        $this->assertEquals($user->id, $decodedUser->id);
    }
}
