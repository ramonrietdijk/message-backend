<?php

namespace Tests\Exceptions;

use App\Exceptions\AlertException;
use App\Utilities\AlertMessage;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class AlertExceptionTest extends TestCase
{
    #[Test]
    public function it_can_render(): void
    {
        $exception = new AlertException('::message::', 500);

        $jsonResponse = $exception->render();

        $this->assertEquals(500, $jsonResponse->getStatusCode());

        $this->assertEquals(
            ['alertMessage' => AlertMessage::danger('::message::')->toArray()],
            $jsonResponse->getData(true)
        );
    }
}
