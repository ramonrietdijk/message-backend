<?php

namespace Tests\Utilities;

use App\Utilities\AlertMessage;
use PHPUnit\Framework\Attributes\Test;
use Tests\TestCase;

class AlertMessageTest extends TestCase
{
    #[Test]
    public function it_can_make_messages(): void
    {
        $this->assertEquals(
            ['type' => '::type::', 'message' => '::message::'],
            AlertMessage::make('::type::', '::message::')->toArray()
        );
    }

    #[Test]
    public function it_can_make_success_messages(): void
    {
        $this->assertEquals(
            ['type' => 'success', 'message' => '::message::'],
            AlertMessage::success('::message::')->toArray()
        );
    }

    #[Test]
    public function it_can_make_danger_messages(): void
    {
        $this->assertEquals(
            ['type' => 'danger', 'message' => '::message::'],
            AlertMessage::danger('::message::')->toArray()
        );
    }

    #[Test]
    public function it_can_make_warning_messages(): void
    {
        $this->assertEquals(
            ['type' => 'warning', 'message' => '::message::'],
            AlertMessage::warning('::message::')->toArray()
        );
    }

    #[Test]
    public function it_can_make_info_messages(): void
    {
        $this->assertEquals(
            ['type' => 'info', 'message' => '::message::'],
            AlertMessage::info('::message::')->toArray()
        );
    }
}
