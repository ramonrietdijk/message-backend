<p align="center"><img src="./art/banner.webp" alt="Message Backend"></p>

# Message Backend

Message is an application where users can register an account and post messages using JSON Web Token authentication. It
is also possible to subscribe to other users in order to get their messages on your feed.

Message consists of two different projects:

- A backend made with Laravel.
- A frontend made with VueJS.

## Installation

In order to use the application you'll have to follow the steps below.

### Installing dependencies

Install the required dependencies of this project.

```shell
composer install
```

### Create the .env file

Create your own **.env** file for configuration. You can do this by copying the example file.

```shell
cp .env.example .env
```

### Set the application key

Generate an application key.

```shell
php artisan key:generate
```

### Set JWT variables

In your **.env** file you will find two JWT variables: ```JWT_SECRET``` and ```JWT_EXPIRE```. These values are required
in order to successfully use JSON Web Tokens for authentication within the application.

The ```JWT_SECRET``` should also be set to a random string of at least 32 characters.

Secondly, the ```JWT_EXPIRE``` is the time in seconds of how long a generated token is valid. It is recommended to keep
this value around 900 seconds, which is 15 minutes.

### Database

Configure your **.env** file to be able to connect to your database. After this is done, you can run the migrations with
the following command:

```shell
php artisan migrate
```

## Testing

Test the functionalities of this project by running the command below.

```shell
php artisan test
```

## License

This project is released under the [MIT](/LICENSE.md) license.
